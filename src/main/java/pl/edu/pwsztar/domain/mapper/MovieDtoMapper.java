package pl.edu.pwsztar.domain.mapper;


import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;



@Component
public class MovieDtoMapper {

    public Movie mapFromDto(CreateMovieDto movieDto){

                Movie movie = new Movie();

                movie.setTitle(movieDto.getTitle());
                movie.setImage(movieDto.getImage());
                movie.setYear(movieDto.getYear());
                return movie;
            }
}